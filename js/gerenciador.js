var estados = ["Acre", "Alagoas", "Amapá", "Amazonas", "Bahia", "Ceará", "Distrito Federal", "Espírito Santo", "Goiás", "Maranhão", "Mato Grosso", "Mato Grosso do Sul", "Minas Gerais", "Pará", "Paraíba", "Paraná", "Pernambuco", "Piauí", "Rio de Janeiro", "Rio Grande do Norte", "Rio Grande do Sul", "Rondônia", "Roraima", "Santa Catarina", "São Paulo", "Sergipe", "Tocantins"];
var capitais = ["Rio Branco", "Maceió", "Macapá", "Manaus", "Salvador", "Fortaleza", "Brasília", "Vitória", "Goiânia", "São Luís", "Cuiabá", "Campo Grande", "Belo Horizonte", "Belém", "João Pessoa", "Curitiba", "Recife", "Teresina", "Rio de Janeiro", "Natal", "Porto Alegre", "Porto Velho", "Boa Vista", "Florianópolis", "São Paulo", "Aracaju", "Palmas"];

var selectQtdJogadores = document.querySelector("#idSelectQtdJogadores");
var btConfirmJogadores = document.querySelector("#idBtConfirmJogadores");

var btEnviarResposta = document.querySelector("#idBtEnviarResposta");

var itemListRanking1 = document.querySelector("#idItemList1");
var itemListRanking2 = document.querySelector("#idItemList2");
var itemListRanking3 = document.querySelector("#idItemList3");
var itemListRanking4 = document.querySelector("#idItemList4");

var nameRanking1 = document.querySelector("#idRanking1");
var nameRanking2 = document.querySelector("#idRanking2");
var nameRanking3 = document.querySelector("#idRanking3");
var nameRanking4 = document.querySelector("#idRanking4");

var textoPontosRanking1 = document.querySelector("#idPontosRanking1");
var textoPontosRanking2 = document.querySelector("#idPontosRanking2");
var textoPontosRanking3 = document.querySelector("#idPontosRanking3");
var textoPontosRanking4 = document.querySelector("#idPontosRanking4");

var textoEstadoAtual = document.querySelector("#idNomeEstado");
var nomeEstadoAtual;
var nomeCapitalAtual;

var campoResposta = document.querySelector("#idResposta");

var textoVezAtualJogador = document.querySelector("#idVezJogador");
var textoRodadaAtual = document.querySelector("#idRodadaAtual");
var textoTotalRodadas = document.querySelector("#idTotalRodadas");

var jogadores = []
var qtdJogadores = 0;
var vezJogadorAtual;
var rodadaAtual = 1;
var totalRodadas = 1;

// Ao mudar a opção(select) da quantidade de jogadores
selectQtdJogadores.addEventListener("change", function (event) {

    document.querySelector("#idDivNomeJogadores").classList.remove("invisible");

    let grupoCamposJogador3 = document.querySelector("#idGrupoCampJog3");
    let campoNomeJogador3 = document.querySelector("#idTerceiroJogador");
    let grupoCamposJogador4 = document.querySelector("#idGrupoCampJog4");
    let campoNomeJogador4 = document.querySelector("#idQuartoJogador");

    let itemSelect = Number(this.value);

    console.log(itemSelect);

    switch (itemSelect) {
        case 2:
            console.log("A opção de 2 jogadores foi selecionada");
            // Campo(s) nome jogador 3
            esconderComponente(grupoCamposJogador3);
            desabilitarComponente(campoNomeJogador3);
            limparConteudoComponente(campoNomeJogador3);
            tornarCampoOpcional(campoNomeJogador3);
            // Campo(s) nome jogador 4
            esconderComponente(grupoCamposJogador4);
            desabilitarComponente(campoNomeJogador4);
            limparConteudoComponente(campoNomeJogador4);
            tornarCampoOpcional(campoNomeJogador4);
            break;
        case 3:
            console.log("A opção de 3 jogadores foi selecionada");
            // Campo(s) nome jogador 3
            exibirComponente(grupoCamposJogador3);
            habilitarComponente(campoNomeJogador3);
            tornarCampoObrigatorio(campoNomeJogador3);
            // Campo(s) nome jogador 4
            esconderComponente(grupoCamposJogador4)
            desabilitarComponente(campoNomeJogador4);
            limparConteudoComponente(campoNomeJogador4);
            tornarCampoOpcional(campoNomeJogador4);
            break;
        case 4:
            console.log("A opção de 4 jogadores foi selecionada");
            // Campo(s) nome jogador 3
            exibirComponente(grupoCamposJogador3);
            habilitarComponente(campoNomeJogador3);
            tornarCampoObrigatorio(campoNomeJogador3);
            // Campo(s) nome jogador 4
            exibirComponente(grupoCamposJogador4);
            habilitarComponente(campoNomeJogador4);
            tornarCampoObrigatorio(campoNomeJogador4);
            break;
        default:
            console.log("A opção da quantidade de jogadores não foi selecionada");
            break;
    }

});

// Ao acionar o botão "Confirmar jogadores"
btConfirmJogadores.addEventListener("click", function (event) {
    event.preventDefault();

    var formConfigJogadores = document.querySelector("#formConfigJogadores");

    var configJogadores = obterConfiguracoesJogadores(formConfigJogadores);

    if (configJogadores.qtdJogadores == "") {
        alert("Escolha a quantidade de jogadores!");
        return;
    }

    if (configJogadores.qtdRodadas == "" || configJogadores.qtdRodadas == null) {
        alert("Escolha a quantidade de rodadas!");
        return;
    }

    if (isNaN(configJogadores.qtdRodadas)) {
        alert("Digite um número para a quantidade de rodadas!");
        return;
    }

    console.log("Quantidade de rodadas: " + configJogadores.qtdRodadas);

    if (configJogadores.qtdRodadas < 1) {
        alert("Digite um número positivo para a quantidade de rodadas!");
        return;
    }

    switch (configJogadores.qtdJogadores) {
        case 2:
            if (configJogadores.nomeJogador1 == "" || configJogadores.nomeJogador2 == "") {
                alert("Preencha o nome de todos os jogadores!");
                return;
            }
            break;
        case 3:
            if (configJogadores.nomeJogador1 == "" || configJogadores.nomeJogador2 == ""
                || configJogadores.nomeJogador3 == "") {
                alert("Preencha o nome de todos os jogadores!");
                return;
            }
            break;
        case 4:
            if (configJogadores.nomeJogador1 == "" || configJogadores.nomeJogador2 == ""
                || configJogadores.nomeJogador3 == "" || configJogadores.nomeJogador4 == "") {
                alert("Preencha o nome de todos os jogadores!");
                return;
            }
            break;
        default:
            alert("Quantidade de jogadores inválida!");
            return;
    }

    let jogador1 = {
        codg: 1,
        nome: configJogadores.nomeJogador1,
        pontuacao: configJogadores.pontuacaoJ1
    }
    let jogador2 = {
        codg: 2,
        nome: configJogadores.nomeJogador2,
        pontuacao: configJogadores.pontuacaoJ2
    }
    let jogador3 = {
        codg: 3,
        nome: configJogadores.nomeJogador3,
        pontuacao: configJogadores.pontuacaoJ3
    }
    let jogador4 = {
        codg: 4,
        nome: configJogadores.nomeJogador4,
        pontuacao: configJogadores.pontuacaoJ4
    }

    totalRodadas = configJogadores.qtdRodadas;

    alterarTextoComponente(textoTotalRodadas, totalRodadas);

    let indiceInicial = aleatorizarPerguntasRespostas();
    nomeEstadoAtual = estados[indiceInicial];
    nomeCapitalAtual = capitais[indiceInicial];

    vezJogadorAtual = jogador1.codg;
    console.log("Vez inicial do jogador com codg: " + vezJogadorAtual)

    alterarTextoComponente(textoEstadoAtual, nomeEstadoAtual);

    qtdJogadores = configJogadores.qtdJogadores;

    switch (qtdJogadores) {
        case 2:
            jogadores = [jogador1, jogador2];
            break;
        case 3:
            jogadores = [jogador1, jogador2, jogador3];
            exibirComponente(itemListRanking3);
            break;
        case 4:
            jogadores = [jogador1, jogador2, jogador3, jogador4];
            exibirComponente(itemListRanking3);
            exibirComponente(itemListRanking4);
            break;
        default:
            alert("Quantidade de jogadores não permitida.")
            return;
    }

    console.table(jogadores);

    let sec = document.getElementById("secConfigJogadores");
    while (sec.firstChild) {
        sec.removeChild(sec.firstChild);
    }

    sec.classList = "";

    let jogo = document.querySelector("#secJogo");
    exibirComponente(jogo);

    habilitarComponente(campoResposta);

    alterarTextoComponente(textoVezAtualJogador, configJogadores.nomeJogador1);
    alterarTextoComponente(nameRanking1, configJogadores.nomeJogador1);
    alterarTextoComponente(nameRanking2, configJogadores.nomeJogador2);
    alterarTextoComponente(nameRanking3, configJogadores.nomeJogador3);
    alterarTextoComponente(nameRanking4, configJogadores.nomeJogador4);
});

function obterConfiguracoesJogadores(formulario) {
    let configJogadores = {
        qtdJogadores: Number(formulario.nmQtdJogadores.value),
        qtdRodadas: Math.ceil(Number(formulario.nmQtdRodadas.value)),
        nomeJogador1: formulario.nmPrimeiroJogador.value,
        pontuacaoJ1: Number(0),
        nomeJogador2: formulario.nmSegundoJogador.value,
        pontuacaoJ2: Number(0),
        nomeJogador3: formulario.nmTerceiroJogador.value,
        pontuacaoJ3: Number(0),
        nomeJogador4: formulario.nmQuartoJogador.value,
        pontuacaoJ4: Number(0)
    }
    return configJogadores;
}

//Quando o jogador envia a resposta...
btEnviarResposta.addEventListener("click", function (event) {
    event.preventDefault();

    var formResposta = document.querySelector("#formResposta");

    let respostaJogador = formResposta.nmResposta.value;

    if (respostaJogador == null || respostaJogador == "") {
        alert("Responda a pergunta para enviá-la!");
        return;
    }

    if (respostaJogador.toUpperCase() === nomeCapitalAtual.toUpperCase()) {
        alert("RESPOSTA CORRETA! :D")
        for (let i = 0; i < jogadores.length; i++) {
            if (jogadores[i].codg == vezJogadorAtual) {
                jogadores[i].pontuacao++;
                console.log("Pontuação atual do jogador " + jogadores[i].nome + ": " + jogadores[i].pontuacao);
                break;
            }
        }
    } else {
        alert("RESPOSTA ERRADA... :(")
    }

    moverJogadoresRanking();
    atualizarRanking();

    let proximaQuestao = aleatorizarPerguntasRespostas();
    nomeEstadoAtual = estados[proximaQuestao];
    nomeCapitalAtual = capitais[proximaQuestao];
    alterarTextoComponente(textoEstadoAtual, nomeEstadoAtual);

    if (vezJogadorAtual <= qtdJogadores) {
        vezJogadorAtual++;
        if (vezJogadorAtual > qtdJogadores) {
            vezJogadorAtual = 1;
            rodadaAtual++;
            alterarTextoComponente(textoRodadaAtual, rodadaAtual);
        }
        let proximoJogador = jogadores.find(jogador => jogador.codg === vezJogadorAtual);
        alterarTextoComponente(textoVezAtualJogador, proximoJogador.nome);
    }

    if (rodadaAtual > totalRodadas) {
        let caixaPergunta = document.querySelector("#idCaixaPergunta");
        let caixaResultadoFinal = document.querySelector("#idCaixaResultadoFinal");
        desabilitarComponente(campoResposta);
        desabilitarComponente(this);
        esconderComponente(caixaPergunta);
        exibirComponente(caixaResultadoFinal);
    }

    limparConteudoComponente(campoResposta);
});

function moverJogadoresRanking() {
    jogadores.sort(function (x, y) {
        if (x.pontuacao > y.pontuacao) {
            return -1
        } else if (x.pontuacao < y.pontuacao) {
            return 1
        } else {
            return 0
        }
    });
    console.table(jogadores)
}

function atualizarRanking() {
    for (let i = 0; i < jogadores.length; i++) {
        let nameRanking = document.querySelector("#idRanking" + (i + 1));
        let textoPontosRanking = document.querySelector("#idPontosRanking" + (i + 1));
        alterarTextoComponente(nameRanking, jogadores[i].nome);
        alterarTextoComponente(textoPontosRanking, jogadores[i].pontuacao);
    }
}

function aleatorizarPerguntasRespostas() {
    return Math.floor(Math.random() * estados.length);
}

function esconderComponente(componente) {
    componente.classList.add("visually-hidden");
}

function exibirComponente(componente) {
    componente.classList.remove("visually-hidden");
}

function desabilitarComponente(componente) {
    componente.disabled = true;
}

function habilitarComponente(componente) {
    componente.disabled = false;
}

function limparConteudoComponente(componente) {
    componente.value = "";
}

function tornarCampoObrigatorio(componente) {
    componente.required = true;
}

function tornarCampoOpcional(componente) {
    componente.required = false;
}

function alterarTextoComponente(componente, texto) {
    componente.innerHTML = texto
}